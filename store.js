import { DB_NAME, DB_VERSION }  from './config.js';

export const store = indexedDB.open(DB_NAME, DB_VERSION);

const updateVersion = (event) => {
  let db = store.result;
  switch(event.oldVersion) {
    case 0:
      console.log('Создаем БД');
      createDB();
      break;
    default:
      console.log('Обновляем БД', db, event.oldVersion);
      break;
  }
}
const createDB = () => {
  let db = store.result;
  db.createObjectStore('users', {keyPath: 'id', autoIncrement: true});
}
const success = () => {
  console.log('БД подключена')
}
const error = () => {
  console.log('Ошибка подключения к БД')
}


store.addEventListener('success', success);
store.addEventListener('error', error);
store.addEventListener('upgradeneeded', updateVersion);