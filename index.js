import { store } from './store.js';

const addUser = () => {
  const db = store.result;
  const transaction = db.transaction("users", "readwrite");
  const users = transaction.objectStore("users");
  const request = users.add({
    'login': 'Aidar',
    'password': 'secret'
  });
  request.onsuccess = function() {
    console.log("Юзер добавлен в хранилище", request.result);
  };
  
  request.onerror = function() {
    console.log("Ошибка", request.error);
  };
}

searchForm.addEventListener('submit', (e) => {
  e.preventDefault();
  
  const db = store.result;
  const transaction = db.transaction("users", "readwrite");
  const users = transaction.objectStore("users");

  const request = users.get(search.valueAsNumber);

  request.onsuccess = function() {
    console.log("Найденный юзер", request.result);
  };
  
  request.onerror = function() {
    console.log("Ошибка", request.error);
  };
})

add.addEventListener('click', addUser);
